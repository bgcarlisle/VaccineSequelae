# Vaccine Sequelae

This Shiny app will look for four CSV files in the root directory, `vitals-dose-1.csv`, `vitals-dose-2.csv`, `meds-dose-1.csv` and `meds-dose-2.csv`, as well as one TXT file, `intro.txt`.

The vitals CSV files should look like this:

```
date,time,hr,temp,arm.pain,general.pain
2021-03-22,09:00,72,35.7,0,0
2021-03-22,10:00,72,35.9,1,0
```

The meds CSV files should look like this:

```
date,time,drug,dose.mg
2021-03-22,19:11,Acetaminophen,500
2021-03-22,21:01,Acetaminophen,500
```

`intro.txt` should contain one or two sentences by way of introduction, e.g.:

```
Dr Benjamin Gregory Carlisle received the AstraZeneca Covid-19 vaccine at 08:57 CET on 2021-03-22 (dose 1) and the Pfizer Covid-19 vaccine at 09:46 on 2021-06-14 (dose 2), both in the left arm. The following represent four measures of self-assessed vaccine tolerability taken hourly (when awake). Please note that the sample size for these data is: n=1.
```
